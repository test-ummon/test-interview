import { VoidResolver, JSONResolver } from "graphql-scalars";
import { ColorResolver } from "./resolvers/color";
import { colors } from "./queries/colors";
import { elements } from "./queries/elements";
import { inventories } from "./queries/inventories";
import { minifigs } from "./queries/minifigs";
import { part_categories } from "./queries/part_categories";
import { parts } from "./queries/parts";
import { sets } from "./queries/sets";
import { themes } from "./queries/themes";

export const resolvers = {
  Void: VoidResolver,
  JSON: JSONResolver,
  Color: ColorResolver,
  Query: {
    colors,
    elements,
    inventories,
    minifigs,
    part_categories,
    parts,
    sets,
    themes,
  },
  // Mutation: {},
};
