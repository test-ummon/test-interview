import { Column, Entity, PrimaryColumn } from "typeorm";

@Entity()
export class Theme {
  @PrimaryColumn("bigint", { name: "id" })
  id: string;

  @Column({ name: "name" })
  name: string;

  @Column({ name: "parent_id" })
  parent_id: string;
}