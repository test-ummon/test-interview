import { Column, Entity, PrimaryColumn } from "typeorm";

@Entity()
export class Set {
  @PrimaryColumn({ name: "set_num" })
  set_num: string;

  @Column({ name: "name" })
  name: string;

  @Column({ name: "year" })
  year: string;

  @Column({ name: "theme_id" })
  theme_id: string;

  @Column({ name: "num_parts" })
  num_parts: string;

  @Column({ name: "img_url" })
  img_url: string;
}