import { Column, Entity, PrimaryColumn } from "typeorm";

@Entity()
export class Inventory_set {
  @Column({ name: "inventory_id" })
  inventory_id: string;

  @Column({ name: "set_num" })
  set_num: string;

  @Column({ name: "quantity" })
  quantity: string;
}