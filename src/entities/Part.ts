import { Column, Entity, PrimaryColumn } from "typeorm";

@Entity()
export class Part {
  @PrimaryColumn({ name: "part_num" })
  part_num: string;

  @Column({ name: "name" })
  name: string;

  @Column({ name: "part_cat_id" })
  part_cat_id: string;

  @Column({ name: "part_material" })
  part_material: string;
}