import { Column, Entity, PrimaryColumn } from "typeorm";

@Entity()
export class Part_category {
  @PrimaryColumn("bigint", { name: "id" })
  id: string;

  @Column({ name: "name" })
  name: string;
}