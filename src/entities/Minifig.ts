import { Column, Entity, PrimaryColumn } from "typeorm";

@Entity()
export class Minifig {
  @PrimaryColumn({ name: "fig_num" })
  fig_num: string;

  @Column({ name: "name" })
  name: string;

  @Column({ name: "num_parts" })
  num_parts: string;

  @Column({ name: "img_url" })
  img_url: string;
}