import { Column, Entity, PrimaryColumn } from "typeorm";

@Entity()
export class Part_relationship {
  @Column({ name: "rel_type" })
  rel_type: string;

  @Column({ name: "child_part_num" })
  child_part_num: string;

  @Column({ name: "parent_part_num" })
  parent_part_num: string;
}