import { Column, Entity, PrimaryColumn } from "typeorm";

@Entity()
export class Inventory {
  @PrimaryColumn("bigint", { name: "id" })
  id: string;

  @Column({ name: "version" })
  version: string;

  @Column({ name: "set_num" })
  set_num: string;
}