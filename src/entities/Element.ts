import { Column, Entity, PrimaryColumn } from "typeorm";

@Entity()
export class Element {
  @PrimaryColumn("bigint", { name: "element_id" })
  element_id: string;

  @Column({ name: "part_num" })
  part_num: string;

  @Column({ name: "color_id" })
  color_id: string;

  @Column({ name: "design_id" })
  design_id: string;
}
