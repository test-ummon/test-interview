import { Column, Entity, PrimaryColumn } from "typeorm";

@Entity()
export class Inventory_part {
  @Column({ name: "inventory_id" })
  inventory_id: string;

  @Column({ name: "part_num" })
  part_num: string;

  @Column({ name: "color_id" })
  color_id: string;

  @Column({ name: "quantity" })
  quantity: string;

  @Column({ name: "is_spare" })
  is_spare: string;

  @Column({ name: "img_url" })
  img_url: string;
}