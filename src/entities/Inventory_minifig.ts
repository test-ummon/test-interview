import { Column, Entity, PrimaryColumn } from "typeorm";

@Entity()
export class Inventory_minifig {
  @Column({ name: "inventory_id" })
  inventory_id: string;

  @Column({ name: "fig_num" })
  fig_num: string;

  @Column({ name: "quantity" })
  quantity: string;
}