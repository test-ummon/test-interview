import { AppDataSource } from "../data-source";
import { Part } from '../entities/Part';

export async function parts(
    _parent: undefined,
    _variables: {}
) {
    // Fetch all parts
    return (
        await AppDataSource.manager.find(Part)
    );
}

export async function part(parent, args, context, info) {
    // Fetch a specific part
    const part = await AppDataSource.manager.findOne(Part, { where: { part_num: args.part_num } });
    return part;
}