import { AppDataSource } from "../data-source";
import { Inventory } from '../entities/Inventory';

export async function inventories(
    _parent: undefined,
    _variables: {}
) {
    //Fetch all inventories
    return (
        await AppDataSource.manager.find(Inventory)
    );
}

export async function inventory(parent, args, context, info) {
    // Fetch a specific inventory
    const inventory = await AppDataSource.manager.findOne(Inventory, { where: { id: args.id } });
    return inventory;
}