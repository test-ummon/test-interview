import { AppDataSource } from "../data-source";
import { Theme } from '../entities/Theme';

export async function themes(
    _parent: undefined,
    _variables: {}
) {
    return (
        //Fetch all themes
        await AppDataSource.manager.find(Theme)
    );
}

export async function theme(parent, args, context, info) {
    // Fetch a specific theme
    const theme = await AppDataSource.manager.findOne(Theme, { where: { id: args.id } });
    return theme;
}