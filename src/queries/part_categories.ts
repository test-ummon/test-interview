import { AppDataSource } from "../data-source";
import { Part_category } from '../entities/Part_category';

export async function part_categories(
    _parent: undefined,
    _variables: {}
) {
    // Fetch all part categories
    return (
        await AppDataSource.manager.find(Part_category)
    );
}

export async function part_category(parent, args, context, info) {
    // Fetch a specific part_category
    const part_category = await AppDataSource.manager.findOne(Part_category, { where: { id: args.id } });
    return part_category;
}