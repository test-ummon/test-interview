import { AppDataSource } from "../data-source";
import { Minifig } from '../entities/Minifig';

export async function minifigs(
    _parent: undefined,
    _variables: {}
) {
    //Fetch all minifigs
    return (
        await AppDataSource.manager.find(Minifig)
    );
}

export async function minifig(parent, args, context, info) {
    // Fetch a specific minifig
    const minifig = await AppDataSource.manager.findOne(Minifig, { where: { fig_num: args.fig_num } });
    return minifig;
}