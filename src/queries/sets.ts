import { AppDataSource } from "../data-source";
import { Set } from '../entities/Set';

export async function sets(
    _parent: undefined,
    _variables: {}
) {
    //Fetch all sets
    return (
        await AppDataSource.manager.find(Set)
    );
}

export async function set(parent, args, context, info) {
    // Fetch a specific set
    const set = await AppDataSource.manager.findOne(Set, { where: { set_num: args.set_num } });
    return set;
}