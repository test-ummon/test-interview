import { AppDataSource } from "../data-source";
import { Element } from "../entities/Element";

export async function elements(
    _parent: undefined,
    _variables: {}
) {
    //Fetch all elements
    return (
        await AppDataSource.manager.find(Element)
    );
}

export async function element(parent, args, context, info) {
    // Fetch a specific element
    const element = await AppDataSource.manager.findOne(Element, { where: { element_id: args.element_id } });
    return element;
}